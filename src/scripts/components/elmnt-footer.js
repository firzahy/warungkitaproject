class ElmntFooter extends HTMLElement{
    connectedCallback(){
        this.render();
    }

    render(){
        this.innerHTML = `
            <div class="container">
                <figure>
                    <img src="/src/images/warungkita/transparent.svg" alt"Icon">
                </figure>
                
                <div class="text-box">
                    <h1>WARTECH</h1>
                    <p> 
                        Email : 
                        <a href="https://gmail.com">
                            firzahaikal2@gmail.com
                        </a>
                    </p>
                    <p>
                        Instagram : 
                        <a href="https://www.instagram.com/firzahaykal_/">
                            @firzahaykal_
                        </a>
                    </p>
                    <p class="last">Made With Love. Developed by Firza Haykal Yusuf</p>
                </div>
            </div>
        `;
    }
}

customElements.define('elmnt-footer', ElmntFooter);